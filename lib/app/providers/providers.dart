
import 'package:frontend/core/notifiers/addPost.notifier.dart';
import 'package:frontend/core/notifiers/authentication.notifier.dart';
import 'package:frontend/core/notifiers/blogs.notifier.dart';
import 'package:frontend/core/notifiers/follower.notifier.dart';
import 'package:frontend/core/notifiers/following.notifier.dart';
import 'package:provider/provider.dart';
import 'package:provider/single_child_widget.dart';

List<SingleChildWidget> providers = [...remoteproviders];

List<SingleChildWidget> remoteproviders = [
  ChangeNotifierProvider(create: (_) => AuthenticationNotifier()),
  ChangeNotifierProvider(create: (_) => FollowerNotifier()),
  ChangeNotifierProvider(create: (_) => FollowingNotifier()),
  ChangeNotifierProvider(create: (_) => BlogNotifier()),
  ChangeNotifierProvider(create: (_) => AddNotifier()),
];

